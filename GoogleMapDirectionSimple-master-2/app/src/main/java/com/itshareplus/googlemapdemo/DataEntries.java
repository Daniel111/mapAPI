package com.itshareplus.googlemapdemo;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import Modules.DataObject;

/**
 * Created by imac on 9/23/16.
 */
public class DataEntries {

    private Context context;

    //public static final String DATA_PATH = "data.json";
    public static final String DATA_PATH = "garas.json";

    public DataEntries(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String readStringFromAsset() {
        String json = null;
        try {
            InputStream is = context.getAssets().open(DATA_PATH);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private List<DataObject> result;
    private JSONArray jsonArray;
    private JSONArray photos;
    private Double Imagehieght;
    private DataObject dataObject;

    public List<DataObject> collectImage(String jsonLink){
        result=new ArrayList<>();
        try {
            jsonArray=new JSONObject(jsonLink).getJSONArray("results");
            for (int i = 0; i < jsonArray.length(); i++) {
                if(!jsonArray.getJSONObject(i).isNull("photos"))
                    photos = jsonArray.getJSONObject(i).getJSONArray("photos");
            }
            for(int i = 0; i<photos.length();i++){
                Imagehieght=photos.getJSONObject(i).getDouble("height");
                dataObject=new DataObject();
                dataObject.setHeight(Imagehieght);
                result.add(dataObject);
            }
        }catch (JSONException e){e.printStackTrace();}
        return result;
    }

    public List<DataObject> collectPlaces(String jsonLink)
    {
        result = new ArrayList<>();
        try {
            jsonArray=new JSONObject(jsonLink).getJSONArray("results");
            double test;
            int length = jsonArray.length();
            JSONObject jsonObjectTmp;
            JSONArray JsonObjectTmp1;
            Double lat;
            Double lng;
            String imagUrl;
            String garasName;
            DataObject dataObject;
            for (int i = 0; i < length; i++) {
                jsonObjectTmp = jsonArray.getJSONObject(i).getJSONObject( "geometry");
                lat=jsonObjectTmp.getJSONObject("location").getDouble("lat");
                lng=jsonObjectTmp.getJSONObject("location").getDouble("lng");
                garasName=jsonArray.getJSONObject(i).getString("name");
                imagUrl=jsonArray.getJSONObject(i).getString("icon");
                dataObject = new DataObject();
                dataObject.setLat(lat);
                dataObject.setImagUrl(imagUrl);
                dataObject.setLng(lng);
                dataObject.setGarasName(garasName);
                result.add(dataObject);
            }

            lat = null;
            lng = null;
            imagUrl=null;
            garasName=null;
            jsonObjectTmp = null;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }
}
