package com.itshareplus.googlemapdemo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import Modules.DataObject;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback{

    private GoogleMap mMap;
    private List<DataObject> dataEntries;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latLng=new LatLng(11.555606,104.874228);
        mMap.addMarker(new MarkerOptions().position(latLng));
        final String url="https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+latLng.latitude+","+latLng.longitude+"&rankby=distance&types=gas_station&key=AIzaSyD6KsxH2-B-ZhCWOQZJpa29ELmICK86BAc";
        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
               // latLng=cameraPosition.target;
               // sendRequest(url,);
            }
        });

//        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//        StrictMode.setThreadPolicy(policy);
//        for (int i=0;i<dataEntries.size();i++){
//            LatLng  pp=new LatLng(dataEntries.get(i).getLat(),dataEntries.get(i).getLng());
//            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pp,0));
//            mMap.addMarker(new MarkerOptions().position(pp).title(dataEntries.get(i).getGarasName()).icon(BitmapDescriptorFactory.fromBitmap(loadImage(dataEntries.get(i).getImgUrl()))));
//        }

    }

    private void setGaras(String json, LatLng latlang)
    {
       // dataEntries=new DataEntries(this).collectPlaces(json);
        for (int i=0;i<dataEntries.size();i++){
            LatLng  pp=new LatLng(dataEntries.get(i).getLat(),dataEntries.get(i).getLng());
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pp,0));
            mMap.addMarker(new MarkerOptions().position(pp).title(dataEntries.get(i).getGarasName()).icon(BitmapDescriptorFactory.fromBitmap(loadImage(dataEntries.get(i).getImgUrl()))));
        }
    }

    protected Bitmap loadImage(String imageSrc) {

        URL imageURL = null;

        Bitmap bitmap = null;
        try {
            imageURL = new URL(imageSrc);
        }

        catch (MalformedURLException e) {
            e.printStackTrace();
        }

        try {
            HttpURLConnection connection = (HttpURLConnection) imageURL
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream inputStream = connection.getInputStream();

            bitmap = BitmapFactory.decodeStream(inputStream);

        } catch (IOException e) {

            e.printStackTrace();
        }

        return bitmap;
    }

    private void sendRequest(final String jsonUrl, final LatLng latLng){

        StringRequest stringRequest = new StringRequest(jsonUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        setGaras(jsonUrl,latLng);
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
