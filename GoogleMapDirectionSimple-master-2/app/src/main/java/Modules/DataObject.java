
package Modules;

/**
 * Created by imac on 9/22/16.
 */
public class DataObject {
    private Double lat;
    private Double lng;
    private String garasName;
    private String imgUrl;
    private Double height;

    public Double getHeight(){return this.height;}
    public void setHeight(Double height){this.height=height;}
    public String getImgUrl(){return imgUrl;}
    public void setImagUrl(String imgUrl){this.imgUrl=imgUrl;}

    public String getGarasName(){return garasName;}
    public void setGarasName(String garasName){this.garasName=garasName;}
    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
